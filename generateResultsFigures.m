%% Running time comparison
% Number of points/dimension
n = [5,8,10,11,12,13,14,15,20,25,50,75,100];

% Running time of Kronecker product method
t_Kron = [3.889,4.004,3.92,3.908,3.913,4.007,4.186,4.054,4.306,4.396,6.733,22.012,53.568];
% Running time of regular GP method
t_GP = [6.989,13.857,37.761,79.322,155.114,350.776,942.727,2534.395];

figure(1)
clf
plot(n.^3,t_Kron,'-b')
xlabel('Number of Data Points (N)')
ylabel('Running Time (s)')
title('GPR\_Grid Running Time')

figure(2)
clf
hold on
plot(n(1:8).^3,t_Kron(1:8),'-b')
plot(n(1:8).^3,t_GP,'-r')
xlabel('Number of Data Points (N)')
ylabel('Running Time (s)')
title('GPR\_Grid vs Conventional Gaussian Process Running Time')
legend('GPR\_Grid','Conventional Gaussian Process')

%% Parameter convergence
%  Must calculate paramVals separately
clear,clc
runDemo
params_True = [100,0.5,1,5];

for i=1:100
    if all(paramVals(:,i)==0)
        paramValsTrunc = paramVals(:,1:(i-1));
        break;
    end
end

iter = 1:(i-1);
figure(4)
clf
subplot(2,1,1)
hold on
plot(iter,paramValsTrunc(2,:),'-b')
plot(iter,params_True(2)*ones(1,(i-1)),':b')
plot(iter,paramValsTrunc(3,:),'-r')
plot(iter,params_True(3)*ones(1,(i-1)),':r')
plot(iter,paramValsTrunc(4,:),'-g')
plot(iter,params_True(4)*ones(1,(i-1)),':g')
xlabel('Iterations')
ylabel('Length Scale (L)')
legend('L_1 estimate','L_1 truth','L_2 estimate','L_2 truth','L_3 estimate','L_3 truth')

subplot(2,1,2)
hold on
plot(iter,paramValsTrunc(1,:),'-k')
plot(iter,params_True(1)*ones(1,(i-1)),':k')
xlabel('Iterations')
ylabel('Noise Precision (\gamma)')
legend('Noise Precision estimate','Noise Precision truth')

suptitle('Parameter estimation')

%% Parameter convergence 2
%  Must calculate paramVals separately
% params_True = [100,0.5,1,5];
% L = 76;
% paramVals2 = paramVals(:,1:L);
% 
% iter = 1:L;
% figure(4)
% clf
% subplot(2,1,1)
% hold on
% plot(iter,paramVals2(2,:),'-b')
% plot(iter,params_True(2)*ones(1,L),':b')
% plot(iter,paramVals2(3,:),'-r')
% plot(iter,params_True(3)*ones(1,L),':r')
% plot(iter,paramVals2(4,:),'-g')
% plot(iter,params_True(4)*ones(1,L),':g')
% xlabel('Iterations')
% ylabel('Length Scale (L)')
% legend('L_1 estimate','L_1 truth','L_2 estimate','L_2 truth','L_3 estimate','L_3 truth')
% 
% subplot(2,1,2)
% hold on
% plot(iter,paramVals2(1,:),'-k')
% plot(iter,params_True(1)*ones(1,L),':k')
% xlabel('Iterations')
% ylabel('Noise Precision (\gamma)')
% legend('Noise Precision estimate','Noise Precision truth')
% 
% suptitle('Parameter estimation')
