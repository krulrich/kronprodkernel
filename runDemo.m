clear; format short e; close all;

%% Generate observations from known GP with kronProdKernel
% initialize parameters:
D = 3; % voxel data
lengthScales = [0.5, 1, 5]; % set lengthScales for each of D kernels
kernels = arrayfun(@(x)SE(x),lengthScales,'un',0); % initialize SE kernels
gamma = 100; % white noise precision

model = kronProdKernelGP(D,kernels,gamma); % create model


numPoints = 40;
% create grid of input locations
X = cell(D,1);
for d = 1:D
    X{d} = linspace(0,10,numPoints);
end
W = binornd(1,.1,numPoints,numPoints,numPoints);
% W = zeros(numPoints,numPoints,numPoints);
model.setMissing(W);

%Sample Data and visualize GP
Y = model.sample(X);
% image = squeeze(Y);
% figure, imshow3Dfull(image)

%Set Missing Values
% W=
%% Create new GP model to fit to observations
modelFit = kronProdKernelGP(D);
modelFit.setMissing(W);

% Fit new model
figure;
algOpts.iters = 100;
algOpts.evalInterval = 5;
plotOpts.plot = true;
plotOpts.color = 'k';
plotOpts.legendNames = {'kronProdKernel'};
plotOpts.ax = gca;
[evals,paramVals] = algorithms.rprop(X,Y,modelFit,algOpts,plotOpts);

%% Compare parameters between model and modelFit


%% Predict
Xnew = cell(D,1);
for d = 1:D
    Xnew{d} = linspace(0,10,100);
end
Y_predict = modelFit.predict(Xnew,X,Y);

image2 = squeeze(Y_predict);
figure, imshow3Dfull(image2)

% 
% %% Visualize GP
% Y_fit = modelFit.sample(X);
% z = [10,20,30];
% 
% figure(1)
% subplot(1,2,1)
% imagesc(Y(:,:,z(1)))
% axis square
% subplot(1,2,2)
% imagesc(Y_fit(:,:,z(1)))
% axis square
% figure(2)
% subplot(1,2,1)
% imagesc(Y(:,:,z(2)))
% axis square
% subplot(1,2,2)
% imagesc(Y_fit(:,:,z(2)))
% axis square
% figure(3)
% subplot(1,2,1)
% imagesc(Y(:,:,z(3)))
% axis square
% subplot(1,2,2)
% imagesc(Y_fit(:,:,z(3)))
% axis square