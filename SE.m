classdef SE < handle
    % The squared exponential (SE) covariance kernel
    
    properties
        % model parameters
        l % length scale
        
        % bounds on moddel parameters
        bounds % upper/lower bounds on length scale
        
        nParams % number of parameters in this kernel
    end
    
    methods
        function self = SE(l,bounds)
            self.l = l;
            
            if ~exist('bounds','var')
                self.bounds = [1e-4,1e4];
            else
                self.bounds = bounds;
            end
            
            self.nParams = 1;
        end
        
        function res = getParams(self)
            res = self.l;
        end
        
        function setParams(self,params)
            self.l = params;
        end
        
        function [lb,ub] = getBounds(self)
            lb = self.bounds(1);
            ub = self.bounds(2);
        end
        
        function res = covMat(self,x1,x2)
            % input:
            %     x1: N-dimensional vector of observation locations
            %     x2: M-dimensional vector of observation locations
            % output:
            %     res: N x M Gram matrix
            x1 = x1(:); 
            x2 = x2(:);
            exp_arg = -(bsxfun(@minus, x1', x2).^2)/(2*self.l^2);
            res = exp(exp_arg);
        end
        
        function res = derivCovMatL(self,x1,x2)
            % input:
            %     x1: N-dimensional vector of observation locations
            %     x2: M-dimensional vector of observation locations
            % output:
            %     res: N x M derivative of Gram matrix wrt length scale
            
            x1 = x1(:); 
            x2 = x2(:);
            res = (bsxfun(@minus,x1',x2).^2)/(self.l^3).*self.covMat(x1,x2);
        end
        
        function res = derivCovMat(self,x1,x2,isNoise)
            %input:
            %    x1: N-dimensional vector of observation locations
            %    x2: N-dimensional vector of observation locations
            %    isNoise: indicator passed for if derivative is wrt noise
            %    (1) or length-scale (0)
            %output:
            %    res: N x M derivative of Gram matrix wrt length scale
            %    corresponding to the dimension of the Gram matrix OR wrt
            %    the noise
            
            res{1,1} = self.derivCovMatL(x1,x2);
%             if ~isNoise
%                 res{1,1} = self.derivCovMatL(x1,x2);
%             else
%                 res = 
%             end
        end
    end
end
