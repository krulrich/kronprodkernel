clear; format short e; close all; clc

%% Load One Data Frame and Set up Grid
D=3;
%load data: using one from of run005_02
V=spm_vol('wrrnV0006.img');
Y = spm_read_vols(V);
numPoints = size(Y);
% create grid of input locations
X = cell(D,1);
for d = 1:D
    X{d} = 1:numPoints(d);
end
image = squeeze(Y);
figure, imshow3Dfull(image)
%set missing values
W = zeros(numPoints);
%% Create New GP Model to Fit to Observations
modelFit = kronProdKernelGP(D);
modelFit.setMissing(W);

% Fit new model
figure;
algOpts.iters = 100;
algOpts.evalInterval = 5;
plotOpts.plot = true;
plotOpts.color = 'k';
plotOpts.legendNames = {'kronProdKernel'};
plotOpts.ax = gca;
[evals,paramVals] = algorithms.rprop(X,Y,modelFit,algOpts,plotOpts);

%% Compare parameters between model and modelFit


%% Predict
epsilon=0.001;
Xnew = cell(D,1);
for d = 1:D
    Xnew{d} = linspace(0+epsilon,10+epsilon,numPoints(d));
end
Y_predict = modelFit.predict(Xnew,X,Y);

%% Visualize GP
Y_fit = modelFit.sample(X);
z = [10,20,30];

% figure(1)
% subplot(1,2,1)
% imagesc(Y(:,:,z(1)))
% axis square
% subplot(1,2,2)
% imagesc(Y_fit(:,:,z(1)))
% axis square
% figure(2)
% subplot(1,2,1)
% imagesc(Y(:,:,z(2)))
% axis square
% subplot(1,2,2)
% imagesc(Y_fit(:,:,z(2)))
% axis square
% figure(3)
% subplot(1,2,1)
% imagesc(Y(:,:,z(3)))
% axis square
% subplot(1,2,2)
% imagesc(Y_fit(:,:,z(3)))
% axis square