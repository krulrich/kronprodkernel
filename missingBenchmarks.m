%% Benchmarks on kronProdKernel GP with missing data
%  Evaluate MSE on hyperparmater estimation and running time for various
%  levels of missingness
clear; format short e; close all;

%% Generate observations from known GP with kronProdKernel
% initialize parameters:
D = 3; % voxel data
lengthScales = [0.5, 1, 5]; % set lengthScales for each of D kernels
kernels = arrayfun(@(x)SE(x),lengthScales,'un',0); % initialize SE kernels
gamma = 100; % white noise precision

model = kronProdKernelGP(D,kernels,gamma); % create model

numPoints = 40;
% create grid of input locations
X = cell(D,1);
for d = 1:D
    X{d} = linspace(0,10,numPoints);
end
Y = model.sample(X);

% Set algorithm parameters
figure;
algOpts.iters = 100;
algOpts.evalInterval = 5;
plotOpts.plot = true;
plotOpts.color = 'k';
plotOpts.legendNames = {'kronProdKernel'};
plotOpts.ax = gca;

% Missingness values to test
p = [0,0.01,0.05,0.1,0.15,0.2,0.25,0.5,0.75];
modelFitParams = nan(D+1,numel(p));
runningTimes = nan(1,numel(p));

for i = 1:numel(p)
    % Keep a consistent data set Y, but vary the number of points missing
    Yw = Y;
    W = binornd(1,p(i),numPoints,numPoints,numPoints);
    model.setMissing(W);
    Yw(W==1) = nan;
    
    tic
    modelFit = kronProdKernelGP(D);
    modelFit.setMissing(W);
    algorithms.rprop(X,Yw,modelFit,algOpts,plotOpts);
    runningTimes(i) = toc;
    
    modelFitParams(:,i) = modelFit.getParams();
end
