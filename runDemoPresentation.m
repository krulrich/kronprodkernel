clear; format short e; close all;

%% Generate observations from known GP with kronProdKernel
% initialize parameters:
D = 3; % voxel data
lengthScales = [0.5, 1, 5]; % set lengthScales for each of D kernels
kernels = arrayfun(@(x)SE(x),lengthScales,'un',0); % initialize SE kernels
gamma = 100; % white noise precision

model = kronProdKernelGP(D,kernels,gamma); % create model


numPoints = 100;
% create grid of input locations
X = cell(D,1);
for d = 1:D
    X{d} = linspace(0,10,numPoints);
end

%Parameters for rprop throughout the script
algOpts.iters = 100;
algOpts.evalInterval = 5;
plotOpts.plot = false;
plotOpts.color = 'k';
plotOpts.legendNames = {'kronProdKernel'};
plotOpts.ax = gca;
    
%% We infer parameters for 10 different sampled GP's to show effectiveness of
%hyperparameter inference (note this is complete grid)
numRuns =10;
for j=1:numRuns
    sprintf('Starting iteration %d of Toy Data', j)
    %Sample Data
    Y = model.sample(X);
    Y_obs = Y;
    % Create new GP model to fit to observations
    modelFit = kronProdKernelGP(D);
    % Fit new model
    [evals,paramVals] = algorithms.rprop(X,Y_obs,modelFit,algOpts,plotOpts);
    % Store inferred hyperparameters
    hyperParams(:,j) = modelFit.getParams;
end

save('hyperParamsComplete', 'hyperParams');

%% Now to run Complete Grid on Brain Data
clear;
% Load One Data Frame and Set up Grid
load('data2.mat')   %Data loaded into variable Y

D = ndims(Y);
numPoints = size(Y);

% create grid of input locations
X = cell(D,1);
for d = 1:D
    X{d} = 1:numPoints(d);
end
Y_obs = Y;

%Parameters for rprop throughout the script
algOpts.iters = 100;
algOpts.evalInterval = 5;
plotOpts.plot = false;
plotOpts.color = 'k';
plotOpts.legendNames = {'kronProdKernel'};
plotOpts.ax = gca;
numRuns = 10

for j=1:numRuns
    sprintf('Starting Iteration %d of Complete Grid on Brain', j)
    % Create new GP model to fit to observations
    tic
    modelFit = kronProdKernelGP(D);
    % Fit new model
    [evals,paramVals] = algorithms.rprop(X,Y_obs,modelFit,algOpts,plotOpts);
    runningTimesComplete(j) = toc;
    % Store inferred hyperparameters
    hyperParamsDataComplete(:,j) = modelFit.getParams;
end

save('hyperParamsDataComplete', 'hyperParamsDataComplete');
save('runningTimesComplete', 'runningTimesComplete')

%% Now to run Incomplete Grid on Brain Data
clear;
% Load One Data Frame and Set up Grid
load('data2.mat')   %Data loaded into variable Y

D = ndims(Y);
numPoints = size(Y);

% create grid of input locations
X = cell(D,1);
for d = 1:D
    X{d} = 1:numPoints(d);
end
Y_obs = Y;

%Parameters for rprop throughout the script
algOpts.iters = 100;
algOpts.evalInterval = 5;
plotOpts.plot = false;
plotOpts.color = 'k';
plotOpts.legendNames = {'kronProdKernel'};
plotOpts.ax = gca;
numRuns = 10

% Missingness values to test
p = [0,0.05,0.1,0.25,0.5,0.75];

for i = 1:numel(p)
    modelFitParams{i} = nan(D+1,numel(p));
    runningTimes{i} = nan(1,numel(p));
    
    for j=1:numRuns
        sprintf('Starting iteration %d of missingness %f on brain data', j,p(i))
        % Keep a consistent data set Y, but vary the number of points missing
        Yw = Y;
        W = binornd(1,p(i),numPoints(1),numPoints(2),numPoints(3));
        Yw(W==1) = nan;
        % Create new GP model to fit to observations
        tic
        modelFit = kronProdKernelGP(D);
        modelFit.setMissing(W);
        % Fit new model
        [evals,paramVals] = algorithms.rprop(X,Yw,modelFit,algOpts,plotOpts);
        runningTimes{i}(j) = toc;
        % Store inferred hyperparameters
        modelFitParams{i}(:,j) = modelFit.getParams;
    end
end

save('paramsIncomplete', 'modelFitParams');
save('runningTimesIncomplete','runningTimes');