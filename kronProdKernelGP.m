classdef kronProdKernelGP < handle
    % kronProdKernelGP   Kronecker Product Kernel class
    %
    % kronProdKernelGP Inherits
    %    handle
    %
    % kronProdKernelGP Methods (of interest):
    %    evaluate - evaluate the log evidence of a batch of observations
    %    gradient - obtain the gradient vector wrt all kernel parameters
    %    K - obtain the full kernel Gram matrix
    %    sample - sample data
    %    predict - infer values of unknown locations
    
    %% Model parameters
    properties
        % Dimensionality
        D           % number of dimensions of data
        kernels     % GP basis kernels in each dimension
        
        % Noise
        gamma       % white noise precision
        updateNoise % predict noise precision during hyperparameter inference
        
        % Missingness
        incomplete  % indicator for if on incomplete grid
        W           % Missing value locations
        M           % Number of non-missing vales
    end
    
    %% Class methods
    methods
        %% Constructor/set up
        function self = kronProdKernelGP(D,kernels,gamma)
            % Constructor
            if nargin > 0
                self.D = D;
                
                if ~exist('kernels','var')
                    self.kernels = arrayfun(@(x)SE(x),rand(self.D,1),'un',0);
                else
                    self.kernels = kernels;
                end
                
                if ~exist('gamma','var'), self.gamma = 1;
                else self.gamma = gamma; end
                
                % Initialize assuming a complete grid
                self.incomplete = 0;
                self.W = [];
                
                self.updateNoise = true;
            end
        end
            
        function setMissing(self,W)
            % Indicate an incomplete grid and mark missing values
            self.W = W;
            self.incomplete = 1;
            self.M = numel(W) - sum(W(:));
        end

        %% Params
        function res = getParams(self)
            res = cellfun(@(x)x.getParams,self.kernels,'un',0);
            res = cell2mat(res');

            if self.updateNoise
                res = [res'; self.gamma]; 
            end
        end

        function res = nParams(self)
            res = numel(self.getParams);
        end
        
        function setParams(self,params)
            ind = 1;
            
            for d = 1:self.D
                nPd = self.kernels{d}.nParams;
                self.kernels{d}.setParams(params(ind:ind+nPd-1));
                ind = ind+nPd;
            end
            
            if self.updateNoise
                self.gamma = params(ind);
            end
        end
        
        function [lb,ub] = getBounds(self)
            [lb,ub] = cellfun(@(x)x.getBounds,self.kernels,'un',0);
            lb = cell2mat(lb');
            ub = cell2mat(ub');
            
            if self.updateNoise
                lb = [lb 1e-1]'; ub = [ub 1e4]'; 
            end
        end
        
        %% Hyperparameter inference
        function res = evaluate(self,X,Y,opts)
            % input:
            %     X: D-dimensional cell array.  Each element is a vector
            %        that contains the Nd locations along that dimension.
            %     Y: D-dimensional tensor containing N  total elements
            %        (this may change with application)
            % output:
            %     res: function evaluation for observations Y
            
            res = self.logEvidence(X,Y);
        end
        
        function res = logEvidence(self,X,Y)
            % input:
            %     X: D-dimensional cell array.  Each element is a vector
            %        that contains the Nd locations along that dimension.
            %     Y: D-dimensional tensor containing N  total elements
            % output:
            %     res: marginal log evidence of observations Y
            
            y = permute(Y,self.D:-1:1);
            y = y(:);
            [KX,Q,V_L] = self.calculateKX_Q_V(X);
            N = numel(V_L);
            
            alpha = self.calculateAlpha(y,KX,Q,V_L);
            
            if ~self.incomplete
                %following algorithm 16, lines 1:8 from Saatchi's dissertation
                logdetK = sum(log(V_L+1/self.gamma));
                res = -1/2*(y'*alpha + logdetK + N*log(2*pi));
                return;
            else
                % We use an approximation of the log-det following Wilson et.
                % al. The eigenvalues of the observed data approximated as
                % (M/N)*(lambda)

                %Note that self.calculateAlpha(y) is different for incomplete
                %grid
                [sortedEigs,~] = sort(V_L, 'descend');
                eigApprox = (self.M/N)*sortedEigs(1:self.M);
                logdetK = sum(log(eigApprox+1/self.gamma));
                y(isnan(y)) = 0;    % Get rid of NaNs 
                res = -1/2*(y'*alpha + logdetK + self.M*log(2*pi));
            end
        end
        
        function grad = gradient(self,X,Y,opts)
            % input:
            %     X: D-dimensional cell array.  Each element is a vector
            %        that contains the Nd locations along that dimension.
            %     Y: D-dimensional tensor containing N  total elements
            %     opts: any necessary input options
            % output:
            %     grad: P x 1 vector of gradients wrt kernel parameters
            
            y = permute(Y,self.D:-1:1);
            y = y(:);
            
            [KX,Q,V_L] = self.calculateKX_Q_V(X);
            alpha = self.calculateAlpha(y,KX,Q,V_L);
            
            grad = nan(self.D+1*self.incomplete,1);
            deriv = cell(1,self.D);
            
            if ~self.incomplete
                % TODO: The below double for loop can be reduced from O(D^2) to
                % O(2D), if derivCovMat takes too long
                for i = 1:(self.D)
                    gam_total = 1;
                    for d=1:self.D
                        if d==i
                            deriv{d} = self.kernels{d}.derivCovMat(X{d},X{d});
                            deriv{d} = deriv{d}{1};
                            gam = diag(Q{d}'*deriv{d}'*Q{d});
                            gam_total = kron(gam_total,gam);
                        else
                            deriv{d} = KX{d};
                            gam = diag(Q{d}'*KX{d}'*Q{d});
                            gam_total = kron(gam_total,gam);
                        end
                    end

                    kappa = self.kronMVprod(deriv, alpha);
                    grad(i) = 1/2*alpha'*kappa - 1/2*sum((1./(V_L+1/self.gamma)).*gam_total);
                end

                if self.updateNoise
                    grad(self.D+1) = -(1/2)*(self.gamma^(-2))*(alpha'*alpha)-1/2*sum(-self.gamma^(-2)*(1./(V_L+1/self.gamma)));
                end
                return;
            else
                N = numel(V_L);
                [sortedEigs,eigIdx] = sort(V_L, 'descend');
                eigApproxNoise = (self.M/N)*sortedEigs(1:self.M)+1/self.gamma;
                
                for i = 1:(self.D)
                    gam_total = 1;
                    for d=1:self.D
                        if d==i
                            deriv{d} = self.kernels{d}.derivCovMat(X{d},X{d});
                            deriv{d} = deriv{d}{1};
                            gam = diag(Q{d}'*deriv{d}'*Q{d});
                            gam_total = kron(gam_total,gam);
                        else
                            deriv{d} = KX{d};
                            gam = diag(Q{d}'*KX{d}'*Q{d});
                            gam_total = kron(gam_total,gam);
                        end
                    end
                    gam_total = (self.M/N)*gam_total(eigIdx(1:self.M));
                    kappa = self.kronMVprod(deriv, alpha);
                    grad(i) = 1/2*alpha'*kappa - 1/2*gam_total'*(1./eigApproxNoise);
                end
                
                if self.updateNoise
                    grad(self.D+1) = -(1/2)*(self.gamma^(-2))*(alpha'*alpha)-1/2*sum(-self.gamma^(-2)*(1./eigApproxNoise));
                end
            end          
        end
        
        %% Sampling and Prediction
        function Y = sample(self,X)
            % input:
            %     X: D-dimensional cell array.  Each element is a vector
            %        that contains the Nd locations along that dimension.
            % output:
            %     Y: D-dimensional random tensor containing N  total 
            %        elements
            
            [~,Q,V_L] = self.calculateKX_Q_V(X);
            
            N = numel(V_L);
            
            y = self.kronMVprod(Q, sqrt(V_L + 1/self.gamma).*randn(N,1));
%             % Example code that needs to be revised for efficient kronProd:
%             y = chol(K)*randn(N,1);
%             Y = reshape(y, self.gridSize(X)');
            Y = permute(reshape(y, self.gridSize(X)'),self.D:-1:1);
      
            if self.incomplete
                Y(self.W==1) = nan;
            end
        end
        
        function [mu,Sigma] = predict(self,Xnew,X,Y)
            % input:
            %     Xnew: M x D matrix of M new observation locations, not
            %        necessarily on the original grid (perhaps handle grid
            %        inputs efficiently as well)
            %     X: D-dimensional cell array.  Each element is a vector
            %        that contains the Nd locations along that dimension.
            %     Y: D-dimensional tensor containing N  total elements
            % output:
            %     mu: M x 1 vector containing the predictive mean
            %     Sigma: M x N matrix containing the predictive covariance
            
            y = permute(Y,self.D:-1:1);
            y = y(:);
            
            [KX,Q,V_L] = self.calculateKX_Q_V(X);
            alpha = self.calculateAlpha(y,KX,Q,V_L);
            
            KM = 1;
            KMN = 1;
            for d = 1:self.D
                KM  = kron(KM,self.kernels{d}.covMat(Xnew{d},Xnew{d}));
                KMN = kron(KMN,self.kernels{d}.covMat(X{d},Xnew{d}));
            end
            mu = KMN*alpha;
            mu = reshape(mu, self.gridSize(Xnew)');
                 
            A = self.kronMMprod(cellfun(@(x) x', Q,'un',0),KMN');
            A = bsxfun(@times,1./(V_L + 1/self.gamma),A);
            A = self.kronMMprod(Q,A);       
            Sigma = KM - KMN*A;
        end
        
        function mu = predictPointByPoint(self,Xnew,X,Y)
            % input:
            %     Xnew: D-dimensional cell array, each containing a length
            %        M vector containing the coordinate of a new
            %        observation, not necessarily on the original grid.
            %     X: D-dimensional cell array.  Each element is a vector
            %        that contains the Nd locations along that dimension.
            %     Y: D-dimensional tensor containing N  total elements
            % output:
            %     mu: M x 1 vector containing the predictive mean
            
            y = permute(Y,self.D:-1:1);
            y = y(:);
            
            [KX,Q,V_L] = self.calculateKX_Q_V(X);
            alpha = self.calculateAlpha(y,KX,Q,V_L);
            
            M_test = numel(Xnew{1});
            mu = nan(M_test,1);
            
            for i = 1:M_test
                KMN = 1;
                for d = 1:self.D
                    KMN = kron(KMN,self.kernels{d}.covMat(X{d},Xnew{d}(i)));
                end
                mu(i) = KMN*alpha;
            end
        end

        %% Helper Methods
        function Gram = K(self,X)
            % input:
            %     X: D-dimensional cell array.  Each element is a vector
            %        that contains the Nd locations along that dimension.
            % output:
            %     Gram: D-dimensional cell array containing Gram matrices
            %        along each dimension.
            
            Gram = cell(1,self.D);
            for d = 1:self.D
                Gram{d} = self.kernels{d}.covMat(X{d},X{d}); %TODO: make Gram
            end
        end
        
        function [Q,V] = eigK(self,K)
            Q = cell(self.D,1);
            V = cell(self.D,1);
            for d = 1:self.D
                [Qd,Vd] = eig(K{d});
                Q{d} = Qd;
                V{d} = diag(Vd);
            end
        end
        
        function [KX,Q,V_L] = calculateKX_Q_V(self,X)
            KX = self.K(X);  %makes Gram matrices along each dimension.
            [Q,V] = self.eigK(KX); %performs eigenvalue decomposition.
                                  %Note that Q is cell array of matrices
                                  %and that V is cell array of vectors that
                                  %are the diagonal of the eigenvalue
                                  %matrix (it's a diagonal matrix)
                                  
            %I wrote this part to essentially do the Kronecker product of 
            %the three diagonal matrices V{d} (V_L is their product)
            %In Saatci, we always need Lambda, not Lambda_d (which is V{d}
            %here).  Note that I don't actually do the Kronecker product,
            %but can instead work only with products of the diagonals (the
            %only nonzero elements) and get the diagonal of Lambda (V_L)
            %out

            V_L = 1;
            for d = 1:self.D
                V_L = kron(V_L,V{d});
            end
        end
        
        function alpha = calculateAlpha(self,y,KX,Q,V_L)
            % alpha = K^-1 * y
            if ~self.incomplete
                alpha = self.kronMVprod(cellfun(@(x) x', Q,'un',0),y);

                alpha = 1./(V_L + 1/self.gamma).*alpha;
                alpha = self.kronMVprod(Q,alpha);
            else
                N = numel(y);
                obs = find(~isnan(y));
                y(isnan(y(:)))=[];
                
                DN = sparse(1:self.M,obs,ones(1,self.M),self.M,N);
                [alpha,~] = pcg(@(z) DN*self.kronMVprod(KX,DN'*z)+(1/self.gamma)*z,y,1e-2,400);
                alpha = DN'*alpha;
            end            
        end
        
        function new = copy(self)
            % Instantiate new object of the same class.
            new = feval(class(self));
            
            % Copy all non-hidden properties.
            p = properties(self);
            for i = 1:length(p)
                if ismethod(self.(p{i}),'copy')
                    new.(p{i}) = self.(p{i}).copy;
                else
                    new.(p{i}) = self.(p{i});
                end
            end
        end

    end
    
    %% Static methods
    methods (Static)
        function alpha = kronMVprod(K,u)
            % Multiply a Kronecker matrix with a vector
            % input:
            %     K: D-dimensional cell array of covariance matrices
            %     u: N-dimensional vector
            % output:
            %     (kron(K{:}))*u
            
            N = length(u);
            D = numel(K);
            x = u;
            
            for d = D:-1:1
                [G_d, ~] = size(K{d});
                X = reshape(x, G_d, N/G_d);
                Z = K{d}*X;
                Z = Z';
                x = Z(:);
            end
            alpha = x;
        end
        
        function res = kronMMprod(K,A)
            % Multiply a Kronecker matrix with a matrix
            % Matrix extension of kronMVprod
            [n,m] = size(A);
            res = zeros(n,m);
            for i = 1:m
                res(:,i) = kronProdKernelGP.kronMVprod(K,A(:,i));
            end
        end
        
        function res = gridSize(X)
            % input:
            %     X: D-dimensional cell array.  Each element is a vector
            %        that contains the Nd locations along that dimension.
            % output:
            %     res: D-dimensional vector containing the size of the grid
            %        along each dimension.
            res = cell2mat(cellfun(@(x)numel(x),X,'un',0));
            if numel(res)==1
                res = [res;1];
            end
        end
    end
end