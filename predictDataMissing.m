clear; format short e; close all; clc

%% Load One Data Frame and Set up Grid
load('data1.mat')   %Data loaded into variable Y
dims = size(Y);
Y = reshape(zscore(Y(:)),dims);

D = ndims(Y);
numPoints = size(Y);

% create grid of input locations
X = cell(D,1);
for d = 1:D
    X{d} = 1:numPoints(d);
end

% visualize the data
image = squeeze(Y);
figure, imshow3Dfull(image)

%% Select values to be missing
W = binornd(1,0.2,numPoints);

imageW = squeeze(W);
figure, imshow3Dfull(imageW)

Y_obs = Y;
Y_obs(W==1) = nan;

imageY_obs = squeeze(Y_obs);
figure, imshow3Dfull(imageY_obs)

%% Create New GP Model to Fit to Observations
modelFit = kronProdKernelGP(D);
modelFit.setMissing(W);

% Fit new model
figure;
algOpts.iters = 100;
algOpts.evalInterval = 5;
plotOpts.plot = true;
plotOpts.color = 'k';
plotOpts.legendNames = {'kronProdKernel'};
plotOpts.ax = gca;
[evals,paramVals] = algorithms.rprop(X,Y_obs,modelFit,algOpts,plotOpts);

%% Predict Missing values
X_pred = cell(D,1);
[W1,W2,W3] = ind2sub(size(W),find(W==1));
% [X_pred{1},X_pred{2},X_pred{3}] = ind2sub(size(W),find(W==1));

X_pred{1} = X{1}(W1);
X_pred{2} = X{2}(W2);
X_pred{3} = X{3}(W3);

mu = modelFit.predictPointByPoint(X_pred,X,Y_obs);

% mu = nan(numel(W1),1);
% 
% for i = 1:numel(W1) 
%     X_pred{1} = X{1}(W1(i));
%     X_pred{2} = X{2}(W2(i));
%     X_pred{3} = X{3}(W3(i));
%     [mu(i),~] = modelFit.predict(X_pred,X,Y_obs);
% end
% [mu,Sigma] = modelFit.predict(X_pred,X,Y_obs);

% Y_pred = reshape(mvnrnd(mu(:),Sigma),[10,10,10]);
% Y_together = Y_obs;
% Y_together(16:25,21:30,11:20) = Y_pred;

Y_together = Y_obs;
Y_together(W==1) = mu;
imageY_tog = squeeze(Y_together);
figure, imshow3Dfull(imageY_tog)




%% Compare with interp3
[X1,X2,X3] = ind2sub(size(W),find(W==0));
V = Y_obs;
V(W==1) = [];
scatInterp = scatteredInterpolant(X1,X2,X3,V(:),'natural');
Vq_nat = scatInterp(X_pred{1},X_pred{2},X_pred{3});
% Y_interp = Y_obs;
% Y_interp(W==1) = Vq;
% imageY_interp = squeeze(Y_interp);
% figure, imshow3Dfull(imageY_interp)


%% Back to toy data
% Generate toy data
D = 2; % voxel data
lengthScales = [0.5, 5]; % set lengthScales for each of D kernels
kernels = arrayfun(@(x)SE(x),lengthScales,'un',0); % initialize SE kernels
gamma = 100; % white noise precision

model = kronProdKernelGP(D,kernels,gamma); % create model

numPoints = 100;
X = cell(D,1);
for d = 1:D
    X{d} = linspace(0,10,numPoints);
end

% Sample data
Y = model.sample(X);
image = squeeze(Y);
figure, imagesc(image)

% Set missing values
W = binornd(1,0.1,numPoints,numPoints);
imageW = squeeze(W);
figure, imagesc(1-imageW)

Y_obs = Y;
Y_obs(W==1) = nan;

imageY_obs = squeeze(Y_obs);
figure, imagesc(imageY_obs)

% Fit new model
modelFit = kronProdKernelGP(D);
modelFit.setMissing(W);

figure;
algOpts.iters = 100;
algOpts.evalInterval = 5;
plotOpts.plot = true;
plotOpts.color = 'k';
plotOpts.legendNames = {'kronProdKernel'};
plotOpts.ax = gca;
[evals,paramVals] = algorithms.rprop(X,Y_obs,modelFit,algOpts,plotOpts);

% Predict Missing values
X_pred = cell(D,1);
[W1,W2] = ind2sub(size(W),find(W==1));

mu = nan(numel(W1),1);

for i = 1:numel(W1) 
    X_pred{1} = X{1}(W1(i));
    X_pred{2} = X{2}(W2(i));
    [mu(i),~] = modelFit.predict(X_pred,X,Y_obs);
end

Y_together = Y_obs;
Y_together(W==1) = mu;
imageY_tog = squeeze(Y_together);
figure, imagesc(imageY_tog)

