\documentclass{beamer}

\mode<presentation>
{
 \usetheme{Warsaw}
 \usecolortheme{default}
 \usefonttheme{professionalfonts}
 \setbeamertemplate{background}[]
 \setbeamertemplate{bibliography item}[text]

}
\usepackage{fancybox}
\usepackage{amsmath,amssymb,cite}
\usepackage{graphicx,times, multirow,grffile}
\usepackage[T1]{fontenc}

\newcommand\Fontvi{\fontsize{10}{7.2}\selectfont}
\newcommand\Fontvii{\fontsize{20}{7.2}\selectfont}

\def\sT{\mathsf T}
\def\bbC{\mathbb C}
\def\bbI{\mathbb I}
\def\bx{\mathbf x}
\def\bg{\mathbf g}
\def\bzero{\mathbf{0}}
\def\bG{\mathbf{G}}
\def\bX{\mathbf{X}}
\def\cC{\mathcal C}
\def\cN{\mathcal N}
\def\det{\mathrm {det}}
\def\by{\mathbf y}
\def\bP{\mathbf P}
\def\bs{\mathbf s}

\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\C}{\mathbb{C}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title[]{Gaussian Processes for fMRI Brain Data}
\author{Gregory Spell, Kevin Liang}
\date{June 17, 2016} 
\normalsize
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}
\subsection{Objectives}
\begin{frame}\frametitle{Objectives}
\begin{itemize}
	\item Modeling fMRI data for brain scans of a subject induced with fear as a GP \footnotemark
	\begin{itemize}
	  \item Can perform regression on a complete and an incomplete grid
	  \item We've succeeded in inferring hyperparameters
	  \item Working on improving prediction of missing points
          \end{itemize}
\end{itemize}
\footnotetext[1]{Ultimately, wish to bridge work regarding LFP data on mice with fMRI data for humans}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Regression}
\begin{frame}[shrink=10]\frametitle{Gaussian Process Regression}
\begin{itemize}
	\item Goal is to find mapping between input-space and output-space given training data:
	\[\mathbf{X} = \{x_n\}_{n=1}^N, x_i \in \mathbb{R}^D \]
	\[ \mathbf{y} = \{y_n\}_{n=1}^N, y_i \in \mathbb{R} \]
	\item Assume observed outputs are noisy:
	\[y = f(x) + \epsilon \]
	\item Noise is assumed to be additive, i.i.d Gaussian, then the covariance of the output becomes:
	\[\mathrm{cov}(\mathbf{x})  = \mathbf{K}_y = K(\mathbf{X}, \mathbf{X}) + \sigma_n^2 I_N \]
\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}\frametitle{GP Regression: Performing Prediction}
\begin{itemize}
	\item Joint distribution between training data $\{\mathbf{X}, \mathbf{y} \}$ and the unknown test pairs $\{\mathbf{X}_{\ast}, \mathbf{f}_{\ast}\}$:
	\[ \begin{bmatrix} \mathbf{y} \\ \mathbf{f}_{\ast} \end{bmatrix} \sim \mathcal{N} \Big( \mathbf{0}, \begin{bmatrix} K(\mathbf{X}, \mathbf{X}) + \sigma_n^2 I_N & K(\mathbf{X},		\mathbf{X}_{\ast}) \\ K(\mathbf{X}_{\ast}, \mathbf{X}) & K(\mathbf{X}_{\ast}, \mathbf{X}_{\ast}) \end{bmatrix} \Big) \]
	\item Using properties of Gaussian distributions, the conditional distribution of $\mathbf{f}_{\ast}$ is:
	\[\mathbf{f}_{\ast} \mid \mathbf{X}, \mathbf{y}, \mathbf{X}_{\ast} \sim \mathcal{N} (\bar{\mathbf{f}}_{\ast}, \mathrm{cov}(\mathbf{f}_{\ast})) \]
	\[\bar{\mathbf{f}}_{\ast} = K(\mathbf{X}_{\ast},\mathbf{X}) \left[ K(\mathbf{X},\mathbf{X}) + \sigma_n^2  I_N \right]^{-1} \mathbf{y} \]
	\[\mathrm{cov} (\mathbf{f}_{\ast}) = K(\mathbf{X}_{\ast},\mathbf{X}_{\ast}) - K(\mathbf{X}_{\ast}, \mathbf{X})\left[ K(\mathbf{X},\mathbf{X}) + \sigma_n^2  I_N \right]^{-1} 		K(\mathbf{X},\mathbf{X}_{\ast}) \]
\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{GP Regression on Multidimensional Grid}
\begin{frame}[shrink=10]\frametitle{Multidimensional Grid and Kronecker Product}
\begin{itemize}
	\item We assume input-points are on a grid (or lattice) described by Cartesian product
	\[\mathbf{X} = \mathbf{X}^{(1)} \times \dots  \mathbf{X}^{(D)} \]
	\item When covariance function is a tensor product, can use Kronecker product to express covariance matrix:
	\[\mathbf{K} = \bigotimes_{d=1}^D \mathbf{K}_d \]
\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[shrink=10]\frametitle{Advantages of Kronecker Covariance Structure}
\begin{itemize}
	\item Efficient (linear runtime) matrix-vector multiplication to perform:
	\[ \boldsymbol{\alpha} = \Big( \bigotimes_{d=1}^D \mathbf{A}_d \Big) \mathbf{b} \]
	\item Eigendecomposition $\mathbf{K} = \mathbf{Q} \boldsymbol{\Lambda} \mathbf{Q}^T$:
	\begin{align*}
	\mathbf{Q} &\equiv \bigotimes_{d=1}^D \mathbf{Q}_d ,  &   \boldsymbol{\Lambda} &\equiv \bigotimes_{d=1}^D \boldsymbol{\Lambda}_d
	\end{align*}
	\item With spherical noise added:
	\[(\mathbf{K} + \sigma_n^2 I_N)^{-1} \mathbf{y} = \mathbf{Q} (\boldsymbol{\Lambda} + \sigma_n^2 I_N)^{-1} \mathbf{Q}^T \mathbf{y} \]
	\begin{itemize} \item Matrix inverse is easy because matrix is diagonal \end{itemize}
\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Inference of Hyperparameters}
\begin{frame}[shrink=5]\frametitle{Log-Marginal Likelihood}
\begin{itemize}
	\item Compute log-marginal likelihood as follows:
	\[\mathrm{log}Z(\theta) = -\frac{1}{2} \big( \mathbf{y}^T \boldsymbol{\alpha} + \sum_{i=1}^N \mathrm{log} (\boldsymbol{\Lambda}(i,i) + \sigma_n^2) + N \mathrm{log}(2\pi) \big) \]
	where
	\[ \boldsymbol{\alpha} \equiv \big( \mathbf{K} + \sigma_n^2 I_N \big)^{-1} \mathbf{y} = \mathbf{Q} (\boldsymbol{\Lambda} + \sigma_n^2 I_N)^{-1} \mathbf{Q}^T \mathbf{y}\]
\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[shrink=5]\frametitle{Gradient Log-Marginal Likelihood}
\begin{itemize}
	\item We require the gradient for optimization via gradient ascent
	\begin{equation*}
	\frac{\partial \mathrm{log} Z(\theta)}{\partial \theta_i} = \frac{1}{2} \mathbf{y}^T \mathbf{K}_y^{-1} \frac{\partial \mathbf{K}}{\partial \theta_i} 		\mathbf{K}_y^{-1} \mathbf{y} - \frac{1}{2} \mathrm{tr} (\mathbf{K}_y^{-1} \frac{\partial \mathbf{K}_y}{\partial \theta_i})
	\label{logMargGrad}
	\end{equation*}
	\item We obtain the derivative as the Kronecker product:
	\[ \frac{\partial \mathbf{K}}{\partial \theta_i} =  \frac{\partial \mathbf{K}_i}{\partial \theta_i} \otimes \Big( \bigotimes_{j \neq i} \mathbf{K}_j \Big) \]
	\item Derivative of the log-marginal likelihood w.r.t noise precision is:
	\[\frac{\partial \mathrm{log} Z(\theta)}{\partial \gamma} = -\frac{1}{2} \gamma^{-2} \boldsymbol{\alpha}^T \boldsymbol{\alpha} - \frac{1}{2} \mathrm{sum} \big( 					(\boldsymbol{\Lambda} + \gamma^{-1} I_N)^{-1} \cdot (-\gamma^{-2} I_N) \big) \]
\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[shrink=10]\frametitle{Comparison of Runtimes}

	\begin{centering}
	\includegraphics[scale=0.8]{RunTimeComparison.png}\\ 
	\centering
	\end{centering}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Incomplete Grid}
\begin{frame}[shrink=5]\frametitle{Incomplete Grid}
\begin{itemize}
	\item Complete grid using \textit{W} imaginary observations: 
	\[\mathbf{y}_W \sim \mathcal{N} (0, \epsilon^{-1} I_W)\]
	and let $\epsilon \to 0$
	\item Now, instead of considering $\mathbf{K}_y = K(\mathbf{X}, \mathbf{X}) + \sigma_n^2 I_N$, we must consider:
	\[\mathbf{K}_y = K(\mathbf{X}, \mathbf{X}) + D_N \]
	where $D_N$ is a diagonal matrix with nonzero entries either $\sigma_n^2$ or $\epsilon^{-1}$. The above cannot efficiently be decomposed using Kronecker structure.
\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[shrink=5]\frametitle{Incomplete Grid}
\begin{itemize}
	\item To evaluate $(K_N + D_N)^{-1} \mathbf{y}$, we use preconditioned conjugate gradient, and employ efficient Kronecker matrix-vector products in PCG
	\item To infer the hyperparameters (marginal log-likelihood and its derivative), we approximate the \textit{M} largest eigenvalues, where \textit{M} is the number of observations, as:
	\[\tilde{\lambda}_i^M = \frac{M}{N} \lambda_i^N \] 
	\item We use the approximated eigenvalues in computing terms in the marginal log-likelihood and its derivative involving sums of the diagonal elements of matrix $\boldsymbol{\Lambda}$
\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Results}
\begin{frame}[shrink=10]\frametitle{Hyperparameter Inference on Toy Data}
\begin{itemize}
\item We sample a GP in three dimensions and infer the known hyperparameters
\end{itemize}
\hspace*{-.1cm}
\begin{table}

\begin{tabular}{| l || c | c |}
\hline
Hyperparameter & true & inferred \\
\hline \hline
length-scale x & 0.50 & 0.50113 \\
\hline
length-scale y  & 1.00 & 0.99819 \\
\hline
length-scale z & 5.00 & 5.0167 \\
\hline
noise precision & 100 & 100.03 \\
\hline
\end{tabular}
\caption{Hyperparameter Inference: Complete Grid}
\end{table}

\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[shrink=10]\frametitle{Hyperparameter Inference on Brain Data}
\begin{itemize}
\item Inference for one frame of fMRI brain data
\end{itemize}
\begin{columns}[c]
\begin{column}{8cm}
\begin{table}
\begin{tabular}{| l || c | c | c | c | c |}
\hline
Hyperparameter & Complete Grid & Missing 5\% & 10 \% & 25 \% & 50 \% \\
\hline \hline
length-scale x & 1.5903 & 1.6148 & 1.6392 & 1.7209 & 1.9420 \\
\hline
length-scale y  & 1.7830 & 1.8138 & 1.8499 & 1.9629 & 2.2898 \\
\hline
length-scale z & 1.6862 & 1.7124 & 1.7460 & 1.8474 & 2.0808 \\
\hline
noise precision & 114.34  & 109.17 & 103.01 & 87.644 & 61.485 \\
\hline
\end{tabular}
\caption{Hyperparameter Inference: Incomplete Grid}
\end{table}
\end{column}
\hspace{5cm}
\end{columns}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[shrink=10]\frametitle{Slice of Brain Image: View A}
	\begin{columns}
	\column{0.5\textwidth}
	\begin{minipage} [c][0.4\textheight]{\linewidth}
	  \centering
	  \vspace{.9cm}
	  \includegraphics[scale=0.23]{../Figures/brain_fullA.png}\\ 
	\end{minipage}
	\begin{minipage}[c][0.4\textheight][c]{\linewidth}
	  \centering
	  \vspace{2cm}
	  \includegraphics[scale=0.23]{../Figures/brain_obsA.png}\\ 
	\end{minipage}
	\column{0.5\textwidth}
	\begin{minipage} [c][0.4\textheight][c]{\linewidth}
	  \centering
	  \vspace{1cm}
	  \includegraphics[scale=0.23]{../Figures/brain_predA.png}\\ 
	\end{minipage}
	\begin{minipage}[c][0.4\textheight][c]{\linewidth}
	 \centering
	 \vspace{1cm}
	 \begin{itemize}
	 \item Top Left: Full Image
	 \item Bottom Left: Image with 25\% removed
	 \item Predicting missing 25\% using conditional means
	 \end{itemize}
	 \end{minipage}
	\end{columns}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[shrink=10]\frametitle{Slice of Brain Image: View C}
	\begin{columns}
	\column{0.5\textwidth}
	\begin{minipage} [c][0.4\textheight]{\linewidth}
	  \centering
	  \vspace{.9cm}
	  \includegraphics[scale=0.23]{../Figures/brain_fullC.png}\\ 
	\end{minipage}
	\begin{minipage}[c][0.4\textheight][c]{\linewidth}
	  \centering
	  \vspace{2cm}
	  \includegraphics[scale=0.23]{../Figures/brain_obsC.png}\\ 
	\end{minipage}
	\column{0.5\textwidth}
	\begin{minipage} [c][0.4\textheight][c]{\linewidth}
	  \centering
	  \vspace{1cm}
	  \includegraphics[scale=0.23]{../Figures/brain_predC.png}\\ 
	\end{minipage}
	\begin{minipage}[c][0.4\textheight][c]{\linewidth}
	 \centering
	 \vspace{1cm}
	 \begin{itemize}
	 \item Top Left: Full Image
	 \item Bottom Left: Image with 25\% removed
	 \item Predicting missing 25\% using conditional means
	 \end{itemize}
	 \end{minipage}
	\end{columns}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[shrink=10]\frametitle{Slice of Brain Image: View S}
	\begin{columns}
	\column{0.5\textwidth}
	\begin{minipage} [c][0.4\textheight]{\linewidth}
	  \centering
	  \vspace{.9cm}
	  \includegraphics[scale=0.23]{../Figures/brain_fullS.png}\\ 
	\end{minipage}
	\begin{minipage}[c][0.4\textheight][c]{\linewidth}
	  \centering
	  \vspace{2cm}
	  \includegraphics[scale=0.23]{../Figures/brain_obsS.png}\\ 
	\end{minipage}
	\column{0.5\textwidth}
	\begin{minipage} [c][0.4\textheight][c]{\linewidth}
	  \centering
	  \vspace{1cm}
	  \includegraphics[scale=0.23]{../Figures/brain_predS.png}\\ 
	\end{minipage}
	\begin{minipage}[c][0.4\textheight][c]{\linewidth}
	 \centering
	 \vspace{1cm}
	 \begin{itemize}
	 \item Top Left: Full Image
	 \item Bottom Left: Image with 25\% removed
	 \item Predicting missing 25\% using conditional means
	 \end{itemize}
	 \end{minipage}
	\end{columns}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[shrink=10]\frametitle{Prediction Comparison to Interpolation Methods}
\hspace*{2cm}
\begin{columns}[c]
\begin{column}{12cm}
\begin{table}
\begin{tabular}{| l || c  |}
\hline
Method & Squared-Error\\
\hline \hline
\textbf{GP Regression} &  \textbf{13.8614}\\
\hline
Nearest Neighb. Interp. & 33.9997 \\
\hline
Linear Interp & 18.6264 \\
\hline
Natural Neighb. Interp. & 16.1334 \\
\hline
\end{tabular}
\caption{Comparison of GP Regression Prediction to Interpolation Methods}
\end{table}
\end{column}
\hspace{5cm}
\end{columns}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
%||*******
%||      ****
%||****
%||      ****
%||*******
\section{Backup Slides}
\begin{frame}\frametitle{}
\begin{center}
\Fontvii
Backup Slides
\end{center}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}\frametitle{Hyperparameters Inference}
\begin{itemize}
	\item To infer the hyperparameters of the model, we need the log-marginal likelihood and its gradient
\begin{equation*}
\mathrm{log} p(\mathbf{y} \mid \mathbf{X}, \theta) = -\frac{1}{2} \mathbf{y}^T \mathbf{K}_y^{-1} \mathbf{y} - \frac{1}{2} \mathrm{log} \vert \mathbf{K}_y \vert - \frac{n}{2} \mathrm{log} 2 \pi
\label{logMarg}
\end{equation*}

\begin{equation*}
\frac{\partial}{\partial \theta_i} \mathrm{log} p(\mathbf{y} \mid \mathbf{X}, \theta) = \frac{1}{2} \mathbf{y}^T \mathbf{K}_y^{-1} \frac{\partial \mathbf{K}}{\partial \theta_i} \mathbf{K}_y^{-1} \mathbf{y} - \frac{1}{2} \mathrm{tr} (\mathbf{K}_y^{-1} \frac{\partial \mathbf{K}_y}{\partial \theta_i})
\label{logMargGrad}
\end{equation*}
\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}\frametitle{Kronecker Product}
\begin{itemize}
	\item Kronecker Product:
	\[\mathbf{A}  \otimes \mathbf{B} = \begin{bmatrix} a_{11}B & \cdots & a_{1n}B \\ \vdots & \ddots & \vdots \\ a_{m1}B & \cdots & a_{mn}B \end{bmatrix} \]
\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[shrink=5]\frametitle{Compute Log-Marginal Likelihood}
\begin{enumerate}
\item For each dimension \textit{d} evaluate the covariance $\mathbf{K}_d$ along the dimension and perform the eigendecomposition to acquire $\mathbf{Q}_d$ and $\boldsymbol{\Lambda}_d$
\item Use \textit{kronMVprod} to compute $\boldsymbol{\alpha} = \Big( \bigotimes_{d=1}^D \mathbf{Q}_d^T \Big) \mathbf{y}$
\item Compute $\boldsymbol{\alpha} = (\boldsymbol{\Lambda} + \sigma_n^2 I_N)^{-1} \boldsymbol{\alpha}$
\item Use \textit{kronMVprod} again to compute $\boldsymbol{\alpha} = \Big( \bigotimes_{d=1}^D \mathbf{Q}_d \Big) \boldsymbol{\alpha}$
\item Compute:
\[\mathrm{log}Z(\theta) = -\frac{1}{2} \big( \mathbf{y}^T \boldsymbol{\alpha} + \sum_{i=1}^N \mathrm{log} (\boldsymbol{\Lambda}(i,i) + \sigma_n^2) + N \mathrm{log}(2\pi) \big) \]
\end{enumerate}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[shrink=5]\frametitle{Compute Log-Marginal Likelihood Derivative}
\begin{enumerate}
\item For the dimension \textit{i} that corresponds to the length-scale we are examining, we take the derivative $\frac{\partial \mathbf{K}_i}{\partial l_i}$
	\begin{enumerate}
	\item Assign $\gamma_i = \mathrm{diag}(\mathbf{Q}_i^T \frac{\partial \mathbf{K}_i^T}{\partial l_i} \mathbf{Q}_i)$
	\end{enumerate}
\item For each other dimension \textit{d} assign $\gamma_d = \mathrm{diag}(\mathbf{Q}_d^T \mathbf{K}_d \mathbf{Q}_d)$
\item Compute $\gamma =  \bigotimes_{d=1}^D \gamma_d$
\item Use \textit{kronMVprod} to compute $\boldsymbol{\kappa} =\left[ \frac{\partial \mathbf{K}_i}{\partial l_i} \otimes \Big(\bigotimes_{d \neq i} \mathbf{K}_d \Big) \right] \boldsymbol{\alpha}$
\item Use \textit{kronMVprod} to compute $\boldsymbol{\alpha} = \Big( \bigotimes_{d=1}^D \mathbf{Q}_d^T \Big) \mathbf{y}$
\item Compute:
\[ \frac{\partial \mathrm{log}Z(\theta)}{\partial l_i} = \frac{1}{2} \boldsymbol{\alpha}^T \boldsymbol{\kappa} - \frac{1}{2} \mathrm{sum}((\boldsymbol{\Lambda} + \sigma_n^2 I_N)^{-1} \gamma) \]
\end{enumerate}
\end{frame}

\end{document}